const express = require('express');
const path = require('path');

// Init App
const app = express();

// Load View Engine
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

//Home Route
app.get('/', function(req, res) {
    res.render('index', {
        label:'Home'
    });
});

// Products Route
app.get('/productos', function(req, res) {
    res.render('get_product', {
        label:'Get Products'
    });
});

// Start Server
app.listen(3000, function(req, res) {
    console.log('Server started on port 3000....');
});